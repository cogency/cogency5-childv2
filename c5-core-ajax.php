<div id="ajax-content" class="c5-ajax-content">
<?php
$autop = true;
if( isset( $wp_query->query_vars['autop'] ) == true) {

    if( $wp_query->query_vars['autop'] == "false") {
		$autop =  false;
    }
}
c5_the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'cogency5' ), 0, null, true, $autop );
?>
</div>